angular.module('app')
	.directive('cell', function(){
		return {
			restrict: 'E',
			transclude: true,
			template: '<div class="cell" ng-transclude></div>'
		};
	})
	.directive('key', function(){
		return {
			restrict: 'E',
			transclude: true,
			template: '<span class="key" ng-transclude></span>'
		};
	}).directive('comment', function(){
		return {
			restrict: 'E',
			transclude: true,
			template: '<span class="comment" ng-transclude></span>'
		};
	}).directive('error', function(){
		return {
			restrict: 'E',
			transclude: true,
			template: '<span class="error" ng-transclude></span>'
		};
	}).directive('indent', function(){
		return {
			restrict: 'E',
			transclude: true,
			template: '<div class="indent" ng-transclude></div><br>'
		};
	}).directive('nav', function(){
		return {
			restrict: 'E',
			template: '<p><a ng-click="prev()">Prev</a><a ng-click="next()">Next</a><a ng-click="navigateToPage(\'start\')">Start</a></p>'
		};
	});