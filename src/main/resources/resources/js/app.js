angular.module('app', ['ngRoute'])
	.config(['$routeProvider', function($routeProvider){
		$routeProvider.when('/index', {
			templateUrl: 'navigation.html',
		}).when('/dataTypes', {
			templateUrl: 'dataTypes.html',
		}).when('/referenceVsValue', {
			templateUrl: 'refVsValue.html',
		}).when('/dynamicExpressions', {
			templateUrl: 'dynamicExpressions.html',
		}).when('/functions', {
			templateUrl: 'functions.html',
		}).when('/types', {
			templateUrl: 'types.html',
		}).otherwise({
			redirectTo: '/index'
		});
	}]);