angular.module('app').controller('mainCtrl', [ '$scope', '$location', function($scope, $location) {
	$scope.pagePaths = [ '/dataTypes', '/referenceVsValue', '/dynamicExpressions', '/functions', '/types' ];
	$scope.next = function() {
		var currentPath = $location.path();
		var currentPageIndex = $scope.pagePaths.indexOf(currentPath);
		if (currentPageIndex !== $scope.pagePaths.length - 1) {
			$scope.navigateToPage($scope.pagePaths[currentPageIndex+1])
		}
	};
	$scope.prev = function() {
		var currentPath = $location.path();
		var currentPageIndex = $scope.pagePaths.indexOf(currentPath);
		if (currentPageIndex !== 0) {
			$scope.navigateToPage($scope.pagePaths[currentPageIndex-1])
		}
	};
	$scope.navigateToPage= function(path) {
		$location.path(path);
	};
}]);